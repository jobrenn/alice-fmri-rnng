# Alice fMRI+RNNG Paper

Paper uses RNNG from Hale et al. 2018 ACL to test for effects of surprisal and distance at fMRI ROI timecourses from Brennan et al. 2016 BRLN.
