Overall the paper looks great and the story is clear! Some minor comments below.

Bottom of page 3: "builts" should be "builds"

Page 5: Cite (Hochreiter and Schmidhuber, 1997) for the LSTM

Page 6: "Recurrant Neural Network Grammars" should be "Recurrent Neural Network Grammars"

Page 8: "As a parser, a RNNG" should be "As a parser, an RNNG"?

Page 8: To my knowledge, the internal implementation that we have at DeepMind is the stack-only version of the RNNG (Kuncoro et al., 2016), which does not use the input buffer and an action history (only the memory stack)

Page 9: "Following Hale et al. (2018), we also explore an alternative architecture which does not include this composition function" we should cite Choe and Charniak (2016) for this model since they came up with it: https://aclweb.org/anthology/D16-1257

Page 11: "from a state-of-the-art LSTM recurrant" should be "from a state-of-the-art LSTM recurrent". I think just replace "recurrant" with "recurrent" throughout the document.

Page 22: Main text: "When surprisal is calculated from a RNNG language model, which does encode hierarchy explicitly, such a model improves fits to fMRI signals recorded in the LPTL", but footnote 4 says: "We observed a negative correlation for RNNG surprisal in the LPTL. We do not here speculate on the interpretation of this unexpected effect.". These two statements seem contradictory?

General comments:
I'm not sure what the standard for journal is usually like, but I think there are some places where illustrative figures could be helpful (e.g. the difference between RNNG and RNNG-comp)
