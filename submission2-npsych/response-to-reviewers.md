---
title: Response to Reviewers for *Localizing syntactic predictions using recurrent neural network grammars*
---

We are very grateful for the thoughtful comments from the Reviewers. Their encouragement to, among other things, make the computational aspects of the model clearer to the readership of *Neuropsychologia* and to better justify our statistical comparisons have, we think, really improved the manuscript. We offer a point-by-point response to the reviews in the letter below where each comment from the reviews is set apart with indentation.

### Editor's Summary

> 1 - The first issue concerns making research in computational linguistics assessible to the readership of a behavioural and cognitive neuroscience journal such as Neuropsychologia. This comes up clearly in multiple points by both reviewers, in particular 'Exposition of methods point' by Reviewer 1 and 'The Models and What they Tell us' point by Reviewer 2. I appreciate that providing exhaustive details on a computational model in every manuscript that uses the model is neither realistic nor rational. However, in the attempt of keeping the field united, conveying basic facts and intuitions that clarify the difference between the models is essential. In particular, how DISTANCE measure is derived from the nature of hierarchy in RNNG and whether it is a proxy to any more familiar syntactic relation such as c- command? I appreciate that the section on Computational Modelling already aims to do exactly this, however perhaps it can be further improved via excellent feedback from the reviewers? Additionally, an example demonstrating the output of RNNG (as well as LSTM) for a sample sentence and, if possible, a graphical representation of DISTANCE could be useful.

We have added an extensive "example-based introduction" to the RNNG in our methods section from pages 9 to 14. We also add a new Fig. 1 and Tables 1--2 to help to illustrate the key features of the model.

> 2 - Reviewer 2 makes a thoughtful point about using word rate as a basis for localiser (see 'Localizer and ROIs'). If I understand correctly, your rationale for such localiser was that 'more words more syntactic processing/linguistic composition/etc'. But indeed the active regions could be active for a variety of other reasons that do not involve syntactic processing, including low level auditory processing or basic lexical access. This concern of course can be resolved via additional analysis (using a different localiser), although there may be other possible ways of addressing it.

We agree that the "word rate" localizer is not specific to sentences - it was originally designed as a "broad brush" language localizer. It was used in prior work just to delineate a superset of regions involved language, from which a subset are presumably involved in higher-level aspects of sentence processing. It was not designed to target regions specific to "sentence processing." Here, we are using that dataset as provided.

We have revised the description on page 8 making explicit that these are "language" ROIs, in a general sense, not "sentence" ROIs.

> 3 - Although the current paper focuses on RNNG vs LSTM comparison, Reviewer 1 (see 'Relation to similar analysis') also urges some comparison of the metrics used in this study vs in you earlier work (Brennan et al 2016). Assuming that you agree that opting for different metrics from study to study is not ideal, please provide justification for your choice and/or additional comparisons.

We have added a new analysis which includes some of the predictors from our earlier work. We discuss this analysis in detail in our response letter below.

> 4 - Reviewer 1 (see 'Difference in significance') points out independent analyses based on SURPRISAL and DISTANCE measures and suggests bringing them into a single model. A clear response to this point would be needed.
I hope that you can address the points above and all remaining points by the reviewers in your revision.

We have added an additional analysis where we include all terms together in a single model. We discuss this analysis in greater detail in the response letter below.


### Reviewer 1

> **Exposition of Methods**

> The core contribution of this paper lies in the details of different language model architectures. I strongly suspect that referral to computational linguistics conference proceedings is not ideal to make these ideas accessible to the readership of Neuropsychologia, a journal in "Behavioral and Cognitive Neuroscience". In my opinion the paper would greatly benefit from an adequate explanation of the language models, and in particular, how they are relevant to human sentence processing. For example, despite studying the cited reference, these issues are not entirely clear to me:


We have added an "Example-based introduction" to the model from pages 9--14 that aims to address the points raised below.

> - How does the input look that the network sees at each time point?

The input is a vector embedding of the given word and a vector embedding of the syntactic context, as illustrated in the (new!) Fig 1.

> - What is the output and how is it interpreted? For training, what exactly is the output compared with (and how is it compared)? My best guess is that it is a sequence of parser actions derived from the parsed text; if this is so, were different kinds of actions treated identically?

The parser makes a set of (stochastic) choices to select one of three parsing actions; the result, or output, of these actions is a syntactic tree. See the new Table 1 for a visualization of this. During training, when the network guesses the wrong parser action, error is back-propogated through the entire system. This detail is now included on pages 9--10.

> - How does the composition function alter underlying representations such that model performance increases?

We have added additional text on pages 11--12 as well as Table 2 to help clarify the composition function. This function re-encodes members of a phrase into a single term on the stack. The alternative (-comp) version keeps individual words and labelled brackets as separate terms.

> - I think it would be really helpful to have a diagram with the different components of the model and the flow of information, in particular, to illustrate the difference between the complete model and the -COMP model.

We thank the reviewer for encouraging us to add more visuals. We have added several diagrams to better illustrate how the model operates. Fig. 1 illustrates the architecture of the model while Tables 1 shows a trace indicating the flow of information. Table 2 compares the representations available to the RNNG, RNNG-comp (and LSTM).

> Furthermore, how confident can we be that the models learned something resembling human syntax from a single book (24,941 words, 10:49)? How accurate were the different models' parses of the left out data? Could the agreement between the model's parse of the left out data and the Stanford parser's gold standard be quantified?

For the broader question, F-scores comparing this RNNG architecture with the gold-standard Penn Treebank are reported in Table 1 of Hale et al. (2018), e.g. F = 88.96 with k = 200.

For our purposes, the key question is the comparison between LSTM, RNNG-COMP and RNNG. To help the reader get a sense of the quality of each of these as language models, we now report their perplexity scores. These scores summarize the average uncertainty of each model when confronted with the text from the first chapter of Alice in Wonderland. As now detailed on page 14, LSTM, RNNG-COMP and the full RNNG show perplexity values of 24.10, 23.16, and 25.01, respectively. While lower than those typically observed in computational linguistics, this likely reflects the relatively small genre-specific training text used (as noted by the Reviewer). These values are *much* smaller than would be expected if the models had not learned any regularities in the text (under a uniform language model, perplexity is equal to vocabulary size and there are about 2900 word types in the present case.) And, interestingly, these show that the models all perform roughly equally in terms of predicting the text itself.

> The discussion section opens "This study aims to narrow down the kind of syntactic information used to guide predictive processing". I think it would be helpful to make the answer to this question more explicit. What can be said beyond the previously discussed distinction of sequential vs. hierarchical processing?

We see how the use of "syntactic information" was a bit vague. The key idea is that explicit encoding of phrase-structure matters. But, the nature of those phrase structures is not probed in this study! We have rewritten the first paragraph of the Discussion to make the first point clearer, and also added to our Conclusion to reinforce that the present study does not probe the specific nature of the hierarchical representations at issue.


> **Relation to similar analyses**

> Reading the introduction and discussion, I have the impression of a somewhat arbitrary exploration of a very large parameter space across different papers (to some extent I think this will be addressed if the specific language models are better explained).

We agree with the Reviewer that the parameter space is very large! In terms of "which models to work with" our approach over several years has been incremental. First we looked at Surprisal with "off-the-shelf" models available to us (these were the context-free grammar and markov models from Brennan et al 2016). We see the current work as an important step forward in that the RNNG (and LSTM baseline) reflect recent advances from NLP. These models include key features necessary for cognitive plausibility, like dealing with ambiguity.

The large space of possible analyses -- only a few of which we can do ourselves -- motivated us to release these fMRI time series (and also the EEG data from Hale et al. 2018 and Brennan & Hale 2019) for other groups to work with.


> Choices that seem poorly motivated to me:

> - What was the motivation for picking only 2 out of the 4 complexity metrics described by Hale et al., 2018? (11:7)

This was a choice of practical expediency. The surprisal and distance effects were the most robust from our 2018 ACL paper (the entropy effects were more sensitive to the beam-size setting) and thus they are the only ones we computed for this fMRI analysis. There are no analyses of the fMRI data that we don't report in the paper.

> - What is the motivation for using word tokens as unit of prediction rather than POS as in the previous analysis (Brennan et al., 2016)? Is predicting word tokens a technological advance over predicting POS? When looking for models of hierarchical structure building, wouldn't it be more appropriate to work with POS, since working with word tokens might "contaminate" the results with influence from lexical peculiarities?

We see word tokens as a methodological advance. Human comprehenders, of course, make predictions about individual words directly. Theoretically, feedback on earlier iterations of this project have highlighted the lexical nature of syntactic rules in most main-stream theories (minimalism, CCG, LFG etc.) and, relatedly, the importance of selectional restrictions that are tied to specified words or classes of words. From our perspective on surprisal, what is "syntactic" is the information that is used to make these predictions (i.e. is the syntactic context a hierarchically structured set of words or a word sequence?)

We now also include an analysis that includes the POS-based surprisal terms from Brennan et al. 2016. One take-away from that analysis is that  POS- and lexical-predictions may play separable roles in incremental processing. But the present work is not itself well suited to teasing apart the contribution of these two kinds of prediction.

> - Rather than using a previously used model as baseline, a new "sequential processing" baseline is used (LSTM; 5:49 ff.). Does the LSTM actually perform better than previously used Ngram models (both in terms of textual predictions and in predictions of brain data)?

In terms of neural predictions, the previous NGram models that were defined over individual lexical items did a poor job predicting the fMRI time series (compare the present results with Fig. 4 from Brennan et al. 2016). Focusing on NGram models over parts-of-speech, these are on a par with the present LSTM model in terms of neural fits, but they cannot be directly compared in terms of textual performance as the LSTM has the (much more difficult) job of predicting individual lexical items.

> Furthermore, given that the most relevant previous results come from the same main authors, and the same dataset, I would expect a more systematic comparison.

We have taken up this challenge and now report an analysis of the effect of the LSTM and RNNG predictors above-and-beyond key predictors from Brennan et al. 2016. The details of this analysis are given in the Supplemental Material. We want to emphasize, though, that we do not see this as a very direct comparison because the earlier modeling effort concerned the much smaller domain of parts-of-speech.


> It seems to me that a crucial test is whether the RNNG distance metric explains neural data beyond the node count metrics used in Brennan et al. (2016). If it does not, what can I conclude from the new analysis? Granted, the RNNG learns something about syntax, and hence it can predict some brain activity related to syntactic processing. But Is the new result interesting beyond that?

The short answer is: yes, it does. These are shown now in Supplemental Table S4. But, we are not sure that this comparison is the crucial test for our research question. The node-count measure was always an implausible (if useful) approach to quantifying structure because it simply ignored ambiguity. We suggest that even if the distance metric captured data as well as, but not better than, the node count measures (which is not the case), it would still be interesting as it does so in a more cognitively plausible way.

> "Brennan et al. (2016) ... also report improved fits of the same kind in other regions including LATL, RATL, and LIFG, which we do not find in the present results." (21:51 - 22:13) If this is relevant at all it should really be tested. Is it a significant difference or just a difference in significance? "We suggest that this di!erence may be due to the more sophisticated baseline LSTM model used in the present study" (22:13). Again, the conjecture that the baseline model is causing the difference should be tested if it is of interest.

We have revised this paragraph in light of the new control analysis that includes predictors from our earlier work. The RNNG-COMP surprisal results are not significant above-and-beyond those previous mdoels of surprisal, but RNNG distance results are.


> **Difference in significance**

> Conclusions should not be drawn from difference in significance. Any suggestion of differences between brain regions is problematic, since different regions were never explicitly compared. For example, "We test here whether activity in these regions is conditioned by similar, or by different, aspects of linguistic context," (3:44) explicitly invokes differences between regions. "effects of explicit hierarchy derived from the recurrent neural network grammar are constrained to the left posterior temporal lobe. (7:23)" strongly implies that this is a contrast between regions.

Thank you for calling us out on making this inference without proper support. We now explicitly test for interactions with region in the following way. For each of three terms, LSTM surprisal, RNNG-comp surprisal, and RNNG distance, we construct a regression model which includes all control and lower-order predictors (just as in the step-wise model comparison) and then we add a main effect of region and interactions between each term (control and target) and region. We also include a random slope term by region. We then conduct a model comparison between this model and one in which we only remove the interaction term between the target predictor and region (e.g. RNNG-distance-by-region).

We have added these analyses to the results section on page 24. We report that there is no significant interaction between LSTM surprisal and ROI ($\chi^2(5) = 4.96, p = 0.421$) but there is support for a significant interaction between RNNG-comp surprisal and region ($\chi^2(5) = 20.37, p = 0.001$) and also between RNNG distance and region ($\chi^2(5) = 18.44, p = 0.002$). These interaction effects lend support to the claim that the hierarchical effects of the RNNG are specific to certain ROIs.


> Relatedly, the analysis of surprisal and distance are conducted independently, each without considering variance explained by the other (14:19). In order to distinguish prediction from composition-related processing, it would be of interest whether each of them has significant explanatory power once the other is accounted for. On the other hand, if distinguishing the two is not of interest, then I would suggest reducing the number of comparisons by combining surprisal and distance, i.e., by performing an ANOVA testing whether adding both variables from the given language model in a single step leads to model improvement.

We have added this new analysis (see page 27, Fig. 3, and Supplemental Table S1). We also conducted the suggested ANOVA comparing a model fit with all surprisal and distance terms to one in which either both RNNG surprisal or both RNNG distance terms are removed:

|       | Removing Distance         | Removing Surprisal           |
| ----- | ---------------------------| --------------------------- |
| LATL  | $\chi^2(2)=35.73, p<0.001$ | $\chi^2(2)=2.07, p=0.355$  |
| RATL  | $\chi^2(2)=04.26, p=0.119$ | $\chi^2(2)=3.85, p=0.146$  |
| LIFG  | $\chi^2(2)=18.17, p<0.001$ | $\chi^2(2)=5.72, p=0.057$  |
| LPTL  | $\chi^2(2)=57.95, p<0.001$ | $\chi^2(2)=9.29, p=0.0096$ |
| LIPL  | $\chi^2(2)=20.11, p<0.001$ | $\chi^2(2)=8.68, p=0.013$  |
| LPreM | $\chi^2(2)=06.29, p=0.043$ | $\chi^2(2)=2.28, p=0.320$  |

The RNNG distance and surprisal effects appear to be independent of each other. We have added included these results as Supplemental Table S1.

> **Minor**

> The manuscript comments on the sign of the model coefficients (18:17). It is indeed surprising that higher surprisal would be associated with less brain activity. The authors' hunch, that this is due to the collinearity between the RNNG and the RNNG-COMP models (18:19), could be evaluated by fitting a model without RNNG-COMP, and testing whether the model coefficient is significantly different from 0 (since ANOVA does not test the sign of the coefficient)?

We're grateful for the reviewer's suggestion here, which we have followed. We fit a regression model to the LIPL ROI data without the LSTM and RNNG-COMP surprisal terms that are highly correlated with RNNG surprisal (r > 0.8). The resulting model shows a positive coefficient for RNNG surprisal ($\beta = 0.025 \pm 0.010$). This is in-line with our hunch, but we hestiate to draw strong conclusions here as the models+data appear insufficient to estimate the sign of the target RNNG effect while also taking into account the lower-order variables.

> Acronyms should be explained (LSTM, PCFG)

We now spell out these acronyms at first mention.


> Figure 1: I don't find the explanation of the model paths with the arrows in the right margin intuitive. It is not clear that the bottom arrow originates at the tip of the top arrow.

We have revised this figure to clarify The forked paths of the step-wise analysis.

> **Typos**

> 2:15: publically -> publicly
> 22:24: Along-side effects these effects ...

Thank you - we have fixed these typos.

### Reviewer 2


> Overall, this is a well written and motivated paper and makes a significant contribution to prediction processing in the brain and I think it should be published. The results have clear importance in demonstrating that a hierarchical model can help account for word surprisal beyond a model based on a sequence of words. Generally, however, I found the interpretation of the results in terms of brain regions rather limited (with exception of LATL and LIFG) and I would like to see those further developed. I also have some questions regarding data analysis that I am confident the authors will be able to address.

We thank the Reviewer for their positive feedback.

> The Models and What they Tell us About Surprisal and Syntactic Processing:

> The data primarily look at how well suprisal (unexpectedness of a word in context) correlates with brain activation at each word in a story. This tells us about the end-of-the-line in top-down prediction. In essence we are learning about how difficult it is to process (encode/access and/or integrate) a word given prior predictions considering the types of information that can be used for prediction. However, it doesn't necessarily tell us about where these predictions arise from. I'm wondering if these computational models could tell us that? And if not, it may be worth mentioning this limitation.

We completely agree that the surprisal taps into the outcome of a complex set of predictive processes. We see the model comparisons in our paper as a way to probe one aspect of where the surprisal effects arise from. When surprisal effects from the RNNG model are observed above-and-beyond those of the LSTM, we take that as evidence that predictions arise from, at least in part, the explicit encoding of phrase structure which sets the RNNG apart.

> I liked the inclusion of different complexity metrics and that the Distance metric seems capable of giving a more direct indication of syntactic processing. However, I think the paper would benefit from further description of the "Abstract hierarchical principles" that it corresponds to, in order for its implications to be clearer to a broader audience. Related to this, I am wondering if these models can capture c-command; this was not completely transparent from my reading (ie, as this particular relational information seems quite critical to at least some aspects of syntactic processing).

We have added a new Table 1 that gives a concrete example of the kind of representations that are returned by the RNNG parser. These correspond to the Penn Treebank scheme, which is a context-free grammar. As constituency is explicitly encoded in the model, tree-relations like c-command and dominance are also available (this has been added around page 10. Also, Kuncoro et al. 2016 (https://arxiv.org/abs/1611.05774) offer a long-form discussion of the kinds of syntactic representations learned by RNNGs.)

> Could it be the case that word sequences (LSTM models) are sufficient to predict performance for particular sentences but not more complex relational information which would require hierarchical models to accurately predict processing? If so, could the story context represent a biased mixture of such sentences and potentially dampen the contribution of hierarchical computational models? Overall the RNN model had limited improvement over the LSTM model, but could it be the case that these effects are dependent on the particular sentence?

Yes, we broadly agree with the Reviewer. Indeed work by Linzen, Baroni, and others have shown that LSTMs do recover certain syntactic dependencies such as subject-verb agreement. This is why we think that LSTMs offer a strong and conservative baseline against which we test the RNNG which bakes hierarchy into its architecture.

> Localizer and ROIs:

> 1. The ROIs were identified based on word rate effects within the individual participants (peaks with t>2). I have a couple of comments on this choice:
1. Could the authors justify the identification of ROIs based on peaks with t>2.

$t>2$ is an arbitrary threshold taken from Brennan et al. (2016) That work also tested results with other thresholds ($t>2.5$, $t>3$) and found no qualitative difference in the pattern of results.

> 2. There is an implicit assumption that there are general language regions that can be tapped into by looking at word rate, which may be the case. However, the localizer seems biased to "word" processing (although clearly not solely) and in conjunction with the localizer being better at localizing some language regions over others, I wonder if it may not be an ideal localizer for syntactic processing.

The "word rate" localizer is definitely *not* a "syntactic localizer". We take it here, as in earlier work, as a kind of more general "language localizer". Our assumption is that syntactic regions will be a proper subset of these regions. We have revised the text on page 8 to make this  clearer.

> For example, the LIFG is not localized in (5/26) ~20% of your participants.

Unfortunately we do not have individual test-retest reliability data for this particular localizer (though at the group level, compare Fig. 5 of Brennan et al. 2016 with Fig. 3 from Brennan et al. 2012). But, our result seems to be in line with work by Fedorenko and colleagues that has examined language localizers systematically. For example, Fig. 1 from Fedorenko et al. 2010 (*J Neurophys*) shows large between-subject variability in the peak of left frontal activity.

> Alternatively, there may simply be more individual variability in LIFG's contribution to language processing. Supplementing the ROI analysis with whole-brain analyses could help here in distinguishing between the limitations of a particular choice of localizer and individual variability in a region's contribution to language processing. Further, it would provide further comparability to previous studies (including Brennan et al., 2016).

We disagree with the Reviewer about the value of a whole-brain analysis in this context. First, Fedorenko and others have argued that variability in LIFG localization poses a critical problem to whole-brain analyses. Second, we see the principle contribution at present being the comparison of models (LSTM, RNNG-COMP, RNNG) in terms of their fit with language-related brain activity. We don't see how adding a whole-brain analysis helps meet that aim.

However, we are sensitive to the critique that our localizer is imperfect. We are keen to hear suggestions for alternative localizers that are compatible with naturalistic presentation.


> Interpretations of ATL in Language processing:

> Do you think the composition effects in LATL reflect more specific concepts (generated by composition) or the actual algorithmic processing? Given Semantic Dementia patients are reported to produce sentences well but demonstrate problems in naming/concept representation, at least initially, would this lean towards its role being the representation of specific concepts?

Our data cannot tease apart phrasal composition effects from semantic specificity effects. This is because we do not have a broad-coverage model for computing the specificity of composed phrases. At least one of us (JRB) is sympathetic to idea supported by data from Pylkkanen, Wilson etc. that LATL effects seem to be related to semantic composition, maybe in opposition to more structural posterior temporal effects.

> Further, do the authors have any thoughts on why this is the only region that is bilaterally represented in Language processing (albeit a priori determined via ROI)?

We do not have any deep insights into the bilateral presentation of the ATL in language processing. It was included in the analysis of Brennan et al. 2016 and, thus, here as well for empirical regions; this region frequently shows up in fMRI contrasts between sentences and word lists (e.g. Stowe et al. 1998; Rogalsky et al. 2009).

> Interpretations of LPTL in language processing:

> LPTL is the only region that coherently shows an advantage of the RNNG model above and beyond the LSTM one. Given this is a rather critical result, I feel there was limited interpretation of this finding. Could the authors elaborate on what this can tell us about the processing mechanisms engag

Unfortunately the Reviewer's full remarks appear to be cut off here. We hope our response gets at the Reviewer's question. Note first that we see effects for RNNG distance above-and-beyond the LSTM results also in the in LATL and LIFG, along-side the LPTL. As to what LPTL is doing in sentence-comprehension? Our data support the inference that this region does ``work'' in a way that is isomorphic, in part, to the actions of our RNNG. But this is only a small finding. We simply don't know the space of plausible models for which a similar complexity  metric could be computed -- such models may differ in architecture (e.g. different encoding for memory), or representations (CCG vs. CFG vs. MG structures, syntactic categories vs. typed lambda terms, localized vs. distributed embeddings of any of these...) There is a lot to be tested. The present data are incremental in that they narrow down the hypothesis space in a very general way by ruling out theories that eschew hierarchical representations all-together.
