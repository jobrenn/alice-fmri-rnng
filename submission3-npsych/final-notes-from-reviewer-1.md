## Changes to manuscript draft sent to neuropsychologia production team

* LaTeX file cleaned of comments, "writeup-cln.tex"
*  Reviewer one raised the following comments that we address in the text sent to the production team for publication.
* Files Changed:
  - supplement.pdf
  - writeup.pdf
  - fig2.pdf
  - writeup-cln.tex
  - writeup.bbl


> Reviewer #1: Thank you for the clarifications, I greatly appreciate the authors' work to make this complex subject matter more accessible. I have no further requests, just a few (potential) corrections for the final version:

> I noticed two summary statements that don't seem to correspond completely to the results: "A separate metric tracking the number of derivational steps taken between words correlates with activity in the left temporal lobe and inferior frontal gyrus, but only when explicit hierarchical composition is factored in." (2:14) In both these regions RNNG-COMP was also significant.

JB: Removed "but only when explicit hierarchical composition is factored in" (NB: derivational steps is ony defined when there is some form of hierarchy)

> "To preview our results, we ﬁnd broad eﬀects across regions for surprisal, but eﬀects of explicit hierarchy derived from the recurrent neural network grammar are constrained to the posterior areas of the left temporal lobe." (7:9) Aren't there also eﬀects of explicit hierarchy in LATL and LIFG?

JB: Rephrased so summary is just about surprisal: "but the influence of explicit hierarchy on surprisal, derived from the recurrent neural network grammar, is constrained to the posterior areas of the left temporal lobe."

> "retion" (8:9)

JB: corrected to "region"

> Figure 2: the relevant (circled) asterisk symbol seems to contradict "There are no signiﬁcant interactions with ROI for RNNG surprisal" (25:3)

JB: Re-ran interaction statistics for rnng-surprisal:roi (n.s.): numbers match what is in the text, Fixed Fig. 2 to match these stats.

> "langauge-related" (23:8)

> "and and" (25:5)

> "together together" (Figure 3 legend)

> "We … describe a control analysis evaluates" (p. 5 of supplementary information) ("that" missing?)

JB: Corrected these four typos

> Tables S4 and S5: the top row is missing the control-model symbol
