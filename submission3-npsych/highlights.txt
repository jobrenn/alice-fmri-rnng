* Word surprisal predicts fMRI activity across perisylvian language regions
* Syntactic structure improves predictions for left posterior temporal activity
* Structural analysis steps correlates with temporal and inferior frontal activity
