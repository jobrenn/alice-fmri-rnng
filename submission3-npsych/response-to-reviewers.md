---
title: Response to Reviewers for *Localizing syntactic predictions using recurrent neural network grammars*
---

We are very grateful for the Reviewer's thoughtful attention -- again -- to our submission. We have made several changes, including new statistical interaction tests between our main predictors and ROI, and a discussion of all staistically reliable results (both positive-going and negative-going). Elsewhere, we have clarified aspects of information flow in the computational model along-side some other more minor points.

We detail these changes in our point-by-point response to the Reviewers, below.

## Reviewer #1:

> The authors have addressed the previous reviews very well. The revised manuscript is doing a great job at motivating the new analyses and situating them in the context of previous results. The additional analyses make the results solid and clarify the advance over the previously reported results. The supplementary material adds information of great value for researches interested in the details.

> I just have a few more specific points for consideration, and some requests for clarification concerning the language models.

> Specific points:

> The methods section states that the main predictors were orthogonalized against word rate (18:45), yet, Table 3 indicates that, as "terms entered as predictors into the hierarchical regression models", they are correlated with word rate.

There are two points of clarification to make here. One is that orthogonalization did not apply to purely control predictors, like word-frequency and sound power. But, it did apply to the target predictors. We've updated the text on Page 19.

As shown on Table 3, there remain small correlations between the target terms and word rate. These reflect, in an unfortunately complex way, timing discrepancies in the story timing between participants. There is <0.5 sec range across participants in terms of when the auditory recording began relative to fMRI scanning; this difference *is* taken into account when deriving word-rate and sound power convolved regressors. But, it is not taken into account for the regressors from the computational models. So, the orthogonalized computational model terms are orthogonalized against a word rate regressor that has not been timing-corrected per participant.

We do not think this discrepancy is consequential: the per participant word rate predictors correlate with the non-corrected predictor at r > 0.98; and the remaining correlations seen in Table 3 are very small.

> The analyses including ROI as predictor add an important confirmation. Why was it only conducted for 3 out of the 5 main predictors? Each of the terms shows significant results in some but not all ROIs, so I think it would be valuable to know the result of the interaction for all of them. I would even suggest adding the result of the interaction as a column to Figure 2.

We have added tests for the interaction between all five target predictors and ROI. They show a significant interactions between ROI and: (i) RNNG-comp surprisal, (ii) RNNG-comp distance, and (iii) RNNG distance, but not between LSTM surprisal or RNNG surprisal. These are now reported on page 24 in the Results, and we have added an annotation to Fig. 2 indicating these interactions as well.

> Discussion (29:22): I was wondering why only some but not all significant regions from Figure 2 are mentioned here. After some cross-checking it appears that only regions with positive regression coefficients are mentioned? Should significant results with negative estimates be distrusted? However, in footnote 4 (p. 30) it is argued that "the combination of data and models appear insufficient to accurately estimate its direction", which implies that the criterion for considering an effect meaningful should not be the sign of the coefficient, but whether it improves model predictions or not?

We agree with the Reviewer that our reasoning indicates that the directionality of all of the more highly correlated terms should be interpreted cautiously. We now discuss all of the results -- both positive, and negative-going, in the relevant paragraphs in the Discussion (pp. 30--33).

> Introduction to RNNG:

> "learned" (9:52) in this context is ambiguous to me: is it learned during training, or continually updated as a text is processed even after training has ended?

We see the confusion here, and think it is fixed by just removing the ambiguous term "learned". The Syntactic Context vector is continuously updated as text is processed.

> "While learned vector representations like Syntactic Context are not necessarily interpretable, it seems likely that one of the things Syntactic Context is representing is the history of previous parser actions." (10:21) This seems to contradict "This Syntactic Context vector is itself determined by a numerical encoding of the current contents of a stack memory" (9:55), which is shown as containing a history of parser actions (Figure 1-A). Is Syntactic Context determined by a stack memory containing previous parser actions, or do we not know for sure whether it contains the history of previous parser actions?

We know that the history of previous parser actions determines the syntactic context vector; this is dictated by the architecture of the network. Where we are cautious is in labeling exactly the contents of the resulting vector. This vector is not directly interpretable (the price of using deep learning!)  But, because of the architecture, we think it likely that the contents include something like a history of previous parser actions.

> Figure 1-A (11): What do the arrows symbolize? (Are they pointers?) It is also slightly confusing that the whole graph is labeled "Syntactic Context", but only one box is labeled "st". Also, the legend makes it seem like st in this example does not contain the "(VP" node?

The arrows in Fig 1A indicate the order in which the RNNG updates a vector representation of the syntactic context as it processes an input sequence. The final such state is $s_t$. We have updated the legend to make it clear that $s_t$ indeed includes "(VP" along with other components of the context.

> Could you clarify the distinction between training the RNNG, using it for parsing, and using it for calculating surprisal? Specifically, it is confusing that in one place the third possible action is described as "c) move on to the next word" (9:50), and in another it is stochastically generating a lexical item (Figure 1-D). Similarly, "the beam search algorithm must take into account the imbalance in probability between structural actions and lexical actions" (13:22) whereas in the preceding description the only lexical action is "c) move on to the next word".

As a parser, the RNNG assigns a probability to every action. Thus computing surprisal from these probabilities is trivial. The actions the parser may take include moving on to the next word; because the RNNG is a generative model for (tree,string)s, this action is termed "generate". (While it bears a resemblance to the "shift" action in a bottom-up shift-reduce parser, appealing to that terminology in our earlier draft probably only added confusion.)

> Could you make more explicit in what way the parser is handicapped through the -COMP modification (p. 12)? My naive interpretation of Table 2 is that it could potentially posit ")NP" without having first encountered "(NP", or that it could contain a structure violating hierarchy like "(VP x (NP y )VP z )NP"?

Whereas the full RNNG (call it "+COMP") can take an action to compress a vector representation of the context into a single term (this is the EndPhrase action in Fig 1B), the -COMP variant cannot perform this action. Instead, it has a different action that adds a special CLOSE PHRASE symbol onto the stack at every point where the +COMP variant would have performed the composition (/compression) action.

In principle, the RNNG could accommodate a non-grammatical sequence like the one mentioned, but in practice it will not do so because they do not exist in the training data.

> Distance metric: "It counts the number of syntactic analyses considered by the parser in terms of individual actions as illustrated in Table 1." (17:19) does "considered" refer to the different candidate parses held in the beam search?

Yes, the Reviewer is right. We have re-worked the text here to make this clearer.

> Finally, I think a diagram of the information flow in the complete architecture could be very helpful. I find diagrams that illustrate the flow of information per iteration like these extremely helpful: http://colah.github.io/posts/2015-08-Understanding-LSTMs/ However I realize that the used architectures might not lend themselves to that kind of illustration, and I don't want to place an unreasonable burden on the authors and delay publication if this does not seem feasible, so please consider this a suggestion rather than a requirement

Fig 1A is indeed a data-flow diagram, similar to those used by Chris Olah. Both the diagrams on that website and ours choose to highlight certain features of the network, and skip over other details (e.g. we skip over the internal mechanisms for "forgetting" aspects of the context).

After considering whether an alternative diagram might be added, we have decided that doing so will add further delay to this project. We will keep the Reviewer's encouragement in mind for a future publication.

> Typo:

> "models" (20:47)

Corrected, thank you.

## Reviewer #2:

> The authors have greatly improved presentation of the models for the readership of Neuropsychologia.

Thank you!

> The complexity of language seems to make its robust and precise localization with a single task untenable - hence, I agree with both the authors revised "broad brush" description of their localizer and being unable to come up with a better alternative. I would have thought, however, that complementary, voxel-based analyses could provide further informative value, particularly since many of the previous related computational papers used such methods.  Nonetheless, the ROI analyses on their own (which identify regions generally established as "language  areas") are of significant value and I'm happy to accept the paper for publication.

> There  are a couple of typos:
> Page 9,  Line 44: choses -> chooses
> Page 32,  Line 21: presents -> presence

We have fixed these typos.

> Lowder et al (2017) is published in Cognitive Science, the reference list refers to PsyArXiv

Thank you -- we have updated this reference.
