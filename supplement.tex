\documentclass[12pt]{article}
\usepackage{geometry} % see geometry.pdf on how to lay out the page. There's lots.
\geometry{letterpaper} % or letter or a5paper or ... etc
%\geometry{landscape} % rotated page geometry
%\usepackage{amsaddr}
%\usepackage{showframe} % uncomment to show margins
%\usepackage{texcount} % count words?

% See the ``Article customise'' template for come common customisations
\usepackage{subfigure}
\usepackage[table]{xcolor}
\usepackage{hyperref}
\usepackage{setspace}
\usepackage{siunitx} % for decimal-aligned tabulars
\usepackage{adjustbox} % for adjustbox environment
\usepackage{graphicx}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\rhead{\textsc{Prediction \& RNNGs}}
%\lhead{\textsc{J.R. Brennan}}
\cfoot{\thepage}
\renewcommand{\headrulewidth}{0pt} % to erase header rule

\usepackage{capt-of}
%\usepackage{multirow}
\usepackage{xspace}
\usepackage{amsmath}
\usepackage{totcount} % for tracking number of figs, tables
\regtotcounter{table}
\regtotcounter{figure}

\usepackage{siunitx}
\usepackage[section]{placeins}
\usepackage[]{natbib}
%\usepackage{linguex} %gb4e not compatible with natbib?
%    http://texdoc.net/texmf-dist/doc/latex/linguex/linguex-doc.pdf
%\usepackage{endnotes}
%\usepackage{setspace}

% setup tikz and elements for diagramming
%\usepackage{tikz}
\usepackage{tikz-qtree}
% http://home.uni-leipzig.de/murphy/handouts/tikz-qtree%20tutorial.pdf
% http://texdoc.net/texmf-dist/doc/latex/tikz-qtree/tikz-qtree-manual.pdf

\usepackage{algpseudocode}
\usepackage{algorithm}

\usepackage{xr}
\externaldocument{writeup}

% define other commands
\newcommand{\todo}[1]{\textcolor{red}{[#1]}}
\newcommand{\surprisal}{\textsc{surprisal}\xspace}
\newcommand{\distance}{\textsc{distance}\xspace}
\newcommand{\nodecount}{\textsc{nodecount}\xspace}
\newcommand{\nocomp}{RNNG$_{-COMP}$\xspace}
\newcommand{\control}{\O$_{LSTM}$\xspace}
\newcommand{\controlS}{\O$^{++}_{LSTM}$\xspace}
\newcommand{\scontxt}{\textsl{Syntactic Context}}
\newcommand{\ngram}{\textsc{NGram}\xspace}
\newcommand{\cfg}{\textsc{CFG}\xspace}
\newcommand{\mg}{\textsc{MG}\xspace}
%\renewcommand{\firstrefdash}{}
% command to count words in document
% (must enable shell escape in call to latex! -- see atom settings for latex package)

% title block
\title{Supplemental Material for \textsl{Localizing syntactic predictions using recurrent neural network grammars}} % 96 chars
\author{
Jonathan R. Brennan\thanks{
Corresponding Author: jobrenn@umich.edu}, \textit{University of Michigan} \\
Chris Dyer, \textit{DeepMind} \\
Adhi Kuncoro, \textit{DeepMind, Oxford University} \\
John T. Hale, \textit{DeepMind, University of Georgia}
}
%\address{Department of Linguistics, University of Michigan, Ann Arbor}
%\email{jobrenn@umich.edu}
%\date{} % delete this line to display the current date

%%% STYLE NOTES
% http://www.sciencemag.org/authors/instructions-preparing-initial-manuscript
% use double-spacing throughout


%%% BEGIN DOCUMENT
\begin{document}
%\doublespacing

\maketitle
\tableofcontents

\renewcommand\thetable{S\arabic{table}}
\renewcommand\thefigure{S\arabic{figure}}
%\setcounter{table}{0}
%\setcounter{figure}{0}

\clearpage

\section{Supplementary data from the main analysis}

%%%%%%%%%%%%%%%%%%%%%%%%
% Beam Search Alorithm %
%%%%%%%%%%%%%%%%%%%%%%%%

\begin{algorithm}
\begin{algorithmic}[1]
\State $\textit{thisword} \gets$ input beam
\State $\textit{nextword} \gets \emptyset$
\While{$|\textit{nextword}| < k$}
    \State{$\textit{fringe} \gets$ \parbox[t]{0.25\textwidth}{successors of all states $s\in \textit{thisword}$ via any parsing action}}
    \State{prune $\textit{fringe}$ to top $k$}
    \State $\textit{thisword} \gets \emptyset$
    \For{each parser state $s\in \textit{ fringe}$}
        \If{$s$ came via a lexical action}
            \State add $s$ to $\textit{nextword}$
        \Else \Comment{must have been structural}
            \State{add $s$ to $\textit{thisword}$}
        \EndIf
    \EndFor
\EndWhile \\
\Return $\textit{nextword}$ pruned to top $k_{\scriptstyle\rm word} \ll k$
\end{algorithmic}
\caption{Word-synchronous beam search with fast-tracking (after \citealp{Stern:2017gf}) taken from \citet{P18-1254}.} \label{alg:wordsynchronous}
\end{algorithm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ANOVA COMPARING DISTANCE AND SURPRISAL WHEN FIT IN A MODEL TOGETHER
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{table}[h!]
  \centering
\begin{tabular}{l l l}
\hline
& Removing \distance & Removing \surprisal \\
\hline
LATL & \(\chi^2(2)=35.73, p<0.001\) &
\(\chi^2(2)=2.07, p=0.355\)\\
RATL & \(\chi^2(2)=04.26, p=0.119\) &
\(\chi^2(2)=3.85, p=0.146\)\\
LIFG & \(\chi^2(2)=18.17, p<0.001\) &
\(\chi^2(2)=5.72, p=0.057\)\\
LPTL & \(\chi^2(2)=57.95, p<0.001\) &
\(\chi^2(2)=9.29, p=0.0096\)\\
LIPL & \(\chi^2(2)=20.11, p<0.001\) &
\(\chi^2(2)=8.68, p=0.013\)\\
LPreM & \(\chi^2(2)=06.29, p=0.043\) &
\(\chi^2(2)=2.28, p=0.320\)\\
\hline
\end{tabular}

\caption{\label{tab:full-model-comparison} To test for the independence of RNNG \surprisal and RNNG \distance effects, we conducted model comparisons starting with a model that included all control terms and all target terms together (see Fig. \ref{fig:full-model} from the main text), and then conducted a likelihood ratio test after removing either both RNNG \distance terms (left-hand column) or both RNNG \surprisal terms (right-hand column). The results indicate that \surprisal effects in the LPTL and \distance effects in LATL, LPTL, LIPL and LIFG are reliable at a threshold of $p < 0.01$ even when included in a model along-side each other. }
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COEFFICIENTS FROM ALL THE MODELS FIT IN THE MAIN ANALYSIS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{all_coefs_all_models.pdf}
    \caption{\label{fig:all-coefs} Regression coefficients ($\beta \pm CI_{99\%}$) from six regression models featured in the primary analysis based on step-wise model comparison (rows) against each of the six ROIs (columns). Row-wise, coefficients are shown for: (Row 1) the baseline model, (row 2) baseline and LSTM \surprisal, (row 3) baseline, LSTM \surprisal, and \nocomp \surprisal, (row 4) baseline, LSTM \surprisal, \nocomp \surprisal and RNNG \surprisal, (row 5) baseline, LSTM \surprisal, and \nocomp \distance, and, finally, (row 6) baseline, LSTM \surprisal, \nocomp \distance, and RNNG \distance.}


\end{figure}

\clearpage


\section{Comparison with models from prior work}

Earlier work with the same dataset \citep{Brennan:2016yu} used a set of computational models and complexity metrics that are theoretically related to the LSTM and RNNG models used here, but differ in several key respects.
Importantly, those previous models were limited as accounts of cognition in that they were defined only over parts-of-speech (POS), not individual lexical items, and they did not address the issue of structural ambiguity.
We briefly review those metrics here and describe a control analysis that evaluates the predictors from the main analysis along-side these earlier models.

\citeauthor{Brennan:2016yu} use two kinds of \surprisal metrics: the first, \ngram, is a sequence-based Markov model defined over POS.
The second, \cfg, defines \surprisal for POS based on hierarchical structure from a one-path ``gold-standard'' context-free phrase-structure tree.
They also define a metric, \nodecount, that counts the number of phrase-structure nodes that are added to the gold-standard tree word-by-word using a top-down enumeration strategy.%
%
\footnote{To keep our comparison manageable, we test four of the nine metrics used by \citep{Brennan:2016yu}: ``3gram.p'', ``cfg.surp'', ``cfg.td'', and ``mg.td''. These four span the range of metric types used in that earlier work.}
%
\nodecount was defined for two different tree structures: the same \cfg structures just described, as well as a hand-built Minimalist Grammar, \mg, based on a generative linguistics textbook \citep{Koopman:2005wa}.
\nodecount is theoretically similar to our \distance metric in that it aims to quantify sentence~processing ``work'' but it is less cognitively plausible in that it does not account for structural ambiguity.

Table \ref{tab:control-correlations} shows pairwise correlations between these additional syntactic predictors and the LSTM and RNNG-based predictors that are introduced in the main text.
Model-comparisons were carried out just as in the main analysis except that these four additional predictors were included in the null baseline model.

The results from this additional analysis are summarized in Tables \ref{tab:control-lstm}, \ref{tab:control-surprisal}, and
\ref{tab:control-distance}.
These tables show that the results for RNNG \distance are qualitatively un-changed when taking into account the earlier \surprisal and \nodecount predictors from \citet{Brennan:2016yu} (Table \ref{tab:control-distance}).
In contrast, the pattern for \nocomp \surprisal is different: we do not see evidence for improvement of model fit in the LPTL (Table \ref{tab:control-surprisal}).
Rather \nocomp \surprisal improves model fits in the RATL but, importantly, the coefficient for this effect shows \emph{reduced} activity in this region for higher \surprisal, $\beta = -0.074,~SE = 0.023$.
It appears, then, that the \surprisal predictors from \citeauthor{Brennan:2016yu} are capturing similar variance to that captured by the \nocomp \surprisal predictor in the LPTL.
LSTM \surprisal also patterns a bit differently, showing model improvements that surpass our threshold for multiple comparisons only in LATL and LIFG (Table \ref{tab:control-lstm}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correlation matrix with old and new syntax predictors %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{table}[h]
  \centering

  \resizebox{\textwidth}{!}{
  \begin{tabular}{l S[table-format=1.2] S[table-format=1.2] S[table-format=1.2] S[table-format=1.2] S[table-format=1.2] S[table-format=1.2] S[table-format=1.2] S[table-format=1.2] S[table-format=1.2]}
  	\hline
  	&
  	\textbf{Ngram} &
  	\textbf{CFG} &
  	\textbf{CFG} &
  	\textbf{MG} &
  	\textbf{LSTM} &
  	\textbf{\nocomp} &
  	\textbf{RNNG} &
  	\textbf{\nocomp} &
  	\textbf{RNNG} \\
  	&
  	\textbf{\surprisal} &
  	\textbf{\surprisal} &
  	\textbf{\nodecount} &
  	\textbf{\nodecount} &
  	\textbf{\surprisal} &
  	\textbf{\surprisal} &
  	\textbf{\surprisal} &
  	\textbf{\distance} &
  	\textbf{\distance} \\
  	\hline
    \textbf{NGram.s} &
    1.00    &
      &
      &
      &
      &
      &
      &
      &
      \\
    \textbf{CFG.s}  &
      \textbf{0.41}   &
      1.00   &
      &
      &
      &
      &
      &
      &
      \\
    \textbf{CFG.n}  &
      0.01 &
      0.05 &
      1.00 &
      &
      &
      &
      &
      &
      \\
    \textbf{MG.n} &
    0.10 &
    0.09 &
    \textbf{0.32} &
    1.00 &
    &
    &
    &
    &
    \\
    \textbf{LSTM.s} &
    \textbf{0.20} &
    \textbf{0.33} &
    -0.05 &
    0.014 &
    1.00 &
    &
    &
    &
    \\
    \textbf{RNNG-c.s} &
    \textbf{0.31} &
    \textbf{0.40} &
    -0.11 &
    0.00 &
    \textbf{0.86} &
    1.00 &
    &
    &
    \\
    \textbf{RNNG.s} &
    \textbf{0.34} &
    \textbf{0.38} &
    -0.15 &
    -0.06 &
    \textbf{0.82} &
    \textbf{0.93} &
    1.00 &
    &
    \\
    \textbf{RNNG-c.d} &
    0.13 &
    0.020 &
    -0.08 &
    0.03 &
    0.12 &
    0.19 &
    0.17 &
    1.00 &
    \\
    \textbf{RNNG.d} &
    0.10 &
    0.03 &
    -0.17 &
    0.013 &
    0.08 &
    \textbf{0.20} &
    \textbf{0.22} &
    \textbf{0.64} &
    1.00 \\

  	\hline
  \end{tabular} } %end resizebox

%\begin{verbatim}
%                trigram.p cfg.surp cfg.td   mg.td lstm256surp k20surpneutered k20surp k20distneutered k20dist
%trigram.p           1.000    0.412  0.013  0.1021       0.199          0.3112   0.342           0.128   0.098
%cfg.surp            0.412    1.000  0.054  0.0917       0.329          0.3988   0.382           0.020   0.030
%cfg.td              0.013    0.054  1.000  0.3238      -0.051         -0.1101  -0.155          -0.078  -0.167
%mg.td               0.102    0.092  0.324  1.0000       0.014          0.0027  -0.058           0.034   0.013
%lstm256surp         0.199    0.329 -0.051  0.0143       1.000          0.8590   0.819           0.121   0.079
%k20surpneutered     0.311    0.399 -0.110  0.0027       0.859          1.0000   0.933           0.195   0.204
%k20surp             0.342    0.382 -0.155 -0.0582       0.819          0.9334   1.000           0.166   0.223
%k20distneutered     0.128    0.020 -0.078  0.0335       0.121          0.1953   0.166           1.000   0.643
%k20dist             0.098    0.030 -0.167  0.0128       0.079          0.2039   0.223           0.643   1.000
%\end{verbatim}

\caption{\label{tab:control-correlations} Correlation matrix (Pearson's $r$) for the target LSTM and RNNG predictors along-side \surprisal and \nodecount based predictors from \citet{Brennan:2016yu}. Off-diagonal cells where $r \geq 0.2$ are indicated in \textbf{bold face}. }

\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% extra CONTROL Predictors: LSTM results %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{table}[h]
  \centering

  \begin{tabular}{l c S[table-format=2.2] S[table-format=1.3, table-comparator=true] l}
  	\hline
  	\textbf{ROI}   &
  	\textbf{LogLik} &
  	\textbf{$\chi^2(1)$} &
  	\textbf{$p$}
  	& \\
  	\hline
  	LATL &  -11485  &  14.94   &  < 0.001 & * \\
  	RATL &  -11192  &  5.3     &  0.021 & \\
  	LIFG &  -10634  &  17.0    &  < 0.001 & * \\
  	LPTL &  -11885  &  6.2     &  0.01 &  \\
  	LIPL &  -11982  &  6.6     &  0.01 &  \\
  	LPreM & -12118  &  4.6     &  0.01 &  \\
  	\hline
    \end{tabular}  % end resizebox

  	\caption{\label{tab:control-lstm} Model comparison results for LSTM \surprisal when additional syntactic predictors from \citet{Brennan:2016yu} are added to the null model.  Asterisks (*) indicate an improvement in model fit that surpasses a bonferroni-corrected $\alpha = 0.05/6 = 0.008\overline{3}$ }

\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% extra CONTROL Predictors: RNNG SURPRISAL results %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{table}[h]

\centering
\begin{tabular}{l l l l S[table-format=1.2] S[table-format=1.3, table-comparator=true] l}
	\hline
	\textbf{ROI} &
	\multicolumn{2}{l}{\textbf{Comparison}} &
	\textbf{LogLik} &
	\textbf{$\chi^2(1)$} &
	\textbf{$p$} &
	\\
	\hline
	LATL & \nocomp & $>$ \controlS           & -11484 & 2.73 & 0.10 & \\
			 & RNNG    & $>$ \nocomp + \controlS & -11483 & 1.99 & 0.16 & \\[0.5em]
	RATL & \nocomp & $>$ \controlS           & -11187 & 10.3 & 0.001 & * \\
			 & RNNG    & $>$ \nocomp + \controlS & -11186 & 0.3 & 0.56 & \\[0.5em]
  LIFG & \nocomp & $>$ \controlS           & -10633 & 1.3 & 0.26 &   \\
	     & RNNG    & $>$ \nocomp + \controlS & -10632 & 3.2 & 0.08 &  \\[0.5em]
  LPTL & \nocomp & $>$ \controlS           & -11885 & 0.2 & 0.66 &  \\
	     & RNNG    & $>$ \nocomp + \controlS & -11885 & 0.2 & 0.70 & \\[0.5em]
  LIPL & \nocomp & $>$ \controlS           & -11981 & 0.6 & 0.08 &   \\
	     & RNNG    & $>$ \nocomp + \controlS & -11978 & 7.3 & 0.01 &  \\[0.5em]
  LPreM & \nocomp & $>$ \controlS           & -12118 & 0.0 & 0.94 &    \\
	      & RNNG    & $>$ \nocomp + \controlS & -12117 & 0.4 & 0.50 &    \\
  \hline
\end{tabular}

% \begin{verbatim}
% cL_b    21 23010 23158 -11484    22968  2.73      1    0.09831 .
% cL_c    22 23010 23165 -11483    22966  1.99      1    0.15837
% cR_b    21 22415 22562 -11187    22373  10.3      1      0.001 **
% cR_c    22 22417 22571 -11186    22373   0.3      1      0.558
% cI_b    21 21309 21454 -10633    21267   1.3      1       0.26
% cI_c    22 21308 21460 -10632    21264   3.2      1       0.08 .
% cP_b    21 23812 23961 -11885    23770   0.2      1       0.66
% cP_c    22 23814 23970 -11885    23770   0.2      1       0.70
% cIP_b    21 24004 24152 -11981    23962   3.2      1       0.08 .
% cIP_c    22 24000 24155 -11978    23956   6.1      1       0.01 *
% cM_b    21 24277 24426 -12118    24235   0.0      1       0.94
% cM_c    22 24279 24434 -12117    24235   0.4      1       0.50

%\end{verbatim}

\caption{\label{tab:control-surprisal} Model comparison statistics for RNNG and \nocomp \surprisal with additional control predictors. \controlS denotes a model fit with the regular control predictors, additional syntactic control predictors from \citet{Brennan:2016yu}, as well as LSTM \surprisal. Asterisks (*) indicate an improvement in model fit that surpasses a bonferroni-corrected $\alpha = 0.05/6 = 0.008\overline{3}$. }

\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% extra CONTROL Predictors: RNNG DISTANCE results %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{table}[h]

\centering
\begin{tabular}{l l l l S[table-format=1.2] S[table-format=1.3, table-comparator=true] l}
	\hline
	\textbf{ROI} &
	\multicolumn{2}{l}{\textbf{Comparison}} &
	\textbf{LogLik} &
	\textbf{$\chi^2(1)$} &
	\textbf{$p$} &
	\\
	\hline
	LATL & \nocomp & $>$ \controlS           & -11483 & 4.2 & 0.04 & \\
			 & RNNG    & $>$ \nocomp + \controlS & -11467 & 33.0 & <0.001 & * \\[0.5em]
	RATL & \nocomp & $>$ \controlS           & -11189 & 6.3 & 0.01 & \\
			 & RNNG    & $>$ \nocomp + \controlS & -11188 & 0.6 & 0.44 & \\[0.5em]
  LIFG & \nocomp & $>$ \controlS          & -10627 & 13.3 & <0.001 & *   \\
	     & RNNG    & $>$ \nocomp + \controlS  & -10624 & 6.9 & 0.008 & * \\[0.5em]
  LPTL & \nocomp & $>$ \controlS           & -11864 & 43.3 & <0.001 & * \\
	     & RNNG    & $>$ \nocomp + \controlS & -11851 & 24.1 & <0.001 & * \\[0.5em]
  LIPL & \nocomp & $>$ \controlS          & -11973 & 19.2 & <0.001 & *   \\
	     & RNNG    & $>$ \nocomp + \controlS & -11972 & 2.1 & 0.15 &  \\[0.5em]
  LPreM & \nocomp & $>$ \controlS           & -12117 & 2.3 & 0.13 &    \\
	      & RNNG    & $>$ \nocomp + \controlS & -12115 & 3.0 & 0.08 &    \\
  \hline
\end{tabular}

% \begin{verbatim}
% cL_d    21 23009 23156 -11483    22967   4.2      1       0.04 *
% cL_e    22 22978 23132 -11467    22934  33.0      1      9e-09 ***
% cR_d    21 22419 22566 -11189    22377   6.3      1       0.01 *
% cR_e    22 22421 22574 -11188    22377   0.6      1       0.44
% cI_d    21 21297 21442 -10627    21255  13.3      1      3e-04 ***
% cI_e    22 21292 21444 -10624    21248   6.9      1      0.008 **
% cP_d    21 23769 23918 -11864    23727  43.3      1      5e-11 ***
% cP_e    22 23747 23903 -11851    23703  24.1      1      9e-07 ***
% cIP_d    21 23988 24136 -11973    23946  19.2      1      1e-05 ***
% cIP_e    22 23988 24143 -11972    23944   2.1      1       0.15
% cM_d    21 24275 24424 -12117    24233   2.3      1       0.13
% cM_e    22 24274 24430 -12115    24230   3.0      1       0.08 .
%\end{verbatim}

\caption{\label{tab:control-distance} Model comparison statistics for RNNG and \nocomp \distance with additional control predictors. \controlS denotes a model fit with the regular control predictors, additional syntactic control predictors from \citet{Brennan:2016yu}, as well as LSTM \surprisal. Asterisks (*) indicate an improvement in model fit that surpasses a bonferroni-corrected $\alpha = 0.05/6 = 0.008\overline{3}$. }

\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COEFFICIENTS: Original controls only %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%
%
% \begin{table}[h]
% 	\centering
% 	\resizebox{\textwidth}{!}{
% 	\begin{tabular}{l S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3	]}
% 		  \hline
% 		 &
% 		 \multicolumn{2}{c}{\textbf{LATL}} &
% 		 \multicolumn{2}{c}{\textbf{RATL}} &
% 		 \multicolumn{2}{c}{\textbf{LIFG}} &
% 		 \multicolumn{2}{c}{\textbf{LPTL}} &
% 		 \multicolumn{2}{c}{\textbf{LIPL}} &
% 		 \multicolumn{2}{c}{\textbf{LPreM}} \\
% 		  \hline
% 	(Intercept) & -0.045 & 0.052 & -0.075 & 0.026 & 0.176 & 0.049 & -0.072 & 0.055 & 0.062 & 0.059 & 0.128 & 0.048 \\
%   sndpwr & -0.007 & 0.012 & 0.015 & 0.012 & -0.016 & 0.013 & 0.071 & 0.012 & 0.009 & 0.012 & 0.004 & 0.012 \\
%   rate & 0.137 & 0.016 & 0.097 & 0.013 & 0.101 & 0.014 & 0.097 & 0.019 & 0.082 & 0.016 & 0.105 & 0.014 \\
%   freq & 0.014 & 0.011 & 0.034 & 0.012 & -0.026 & 0.012 & 0.001 & 0.011 & 0.052 & 0.011 & 0.039 & 0.011 \\
%   breaks & 0.169 & 0.011 & 0.070 & 0.012 & 0.015 & 0.012 & -0.028 & 0.011 & -0.081 & 0.011 & 0.008 & 0.011 \\
%   dx & 0.119 & 0.018 & 0.008 & 0.012 & 0.163 & 0.019 & 0.290 & 0.017 & 0.309 & 0.017 & 0.225 & 0.018 \\
%   dy & -0.153 & 0.017 & 0.025 & 0.012 & 0.087 & 0.020 & 0.132 & 0.017 & 0.003 & 0.017 & 0.089 & 0.017 \\
%   dz & 0.107 & 0.027 & 0.097 & 0.023 & -0.260 & 0.028 & 0.174 & 0.026 & 0.016 & 0.027 & -0.091 & 0.025 \\
%   rx & 0.070 & 0.017 & -0.043 & 0.012 & -0.033 & 0.018 & -0.071 & 0.017 & 0.068 & 0.017 & -0.010 & 0.016 \\
%   ry & -0.192 & 0.018 & 0.009 & 0.012 & 0.061 & 0.019 & 0.021 & 0.018 & -0.018 & 0.018 & 0.055 & 0.018 \\
%   rz & 0.125 & 0.019 & -0.006 & 0.015 & -0.115 & 0.018 & 0.022 & 0.018 & 0.100 & 0.019 & 0.048 & 0.018 \\
% 	   \hline
%
% 	\end{tabular} } % end resizebox
% 	\caption{\label{tab:nullcoefs} Regression coefficients ($\pm$ standard error) for models with just the control coefficients fit against data from each ROI.}
% \end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COEFFICIENTS: Add LSTM step-wise %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \begin{table}[h]
% 	\centering
% 	\resizebox{\textwidth}{!}{
% 	\begin{tabular}{l S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3	]}
% 		  \hline
% 		 &
% 		 \multicolumn{2}{c}{\textbf{LATL}} &
% 		 \multicolumn{2}{c}{\textbf{RATL}} &
% 		 \multicolumn{2}{c}{\textbf{LIFG}} &
% 		 \multicolumn{2}{c}{\textbf{LPTL}} &
% 		 \multicolumn{2}{c}{\textbf{LIPL}} &
% 		 \multicolumn{2}{c}{\textbf{LPreM}} \\
% 		  \hline
% (Intercept) & -0.046 & 0.052 & -0.075 & 0.026 & 0.176 & 0.049 & -0.073 & 0.054 & 0.061 & 0.058 & 0.127 & 0.048 \\
% sndpwr & -0.011 & 0.012 & 0.013 & 0.012 & -0.020 & 0.013 & 0.067 & 0.012 & 0.007 & 0.012 & 0.002 & 0.012 \\
% rate & 0.137 & 0.016 & 0.097 & 0.013 & 0.102 & 0.014 & 0.098 & 0.019 & 0.083 & 0.016 & 0.105 & 0.014 \\
% freq & 0.021 & 0.011 & 0.038 & 0.012 & -0.019 & 0.012 & 0.008 & 0.011 & 0.057 & 0.011 & 0.043 & 0.011 \\
% breaks & 0.159 & 0.011 & 0.065 & 0.012 & 0.006 & 0.012 & -0.036 & 0.011 & -0.087 & 0.011 & 0.003 & 0.011 \\
% dx & 0.118 & 0.018 & 0.008 & 0.012 & 0.162 & 0.018 & 0.289 & 0.017 & 0.308 & 0.017 & 0.224 & 0.018 \\
% dy & -0.153 & 0.017 & 0.025 & 0.012 & 0.088 & 0.020 & 0.132 & 0.017 & 0.004 & 0.017 & 0.090 & 0.017 \\
% dz & 0.108 & 0.027 & 0.097 & 0.023 & -0.259 & 0.028 & 0.176 & 0.026 & 0.018 & 0.027 & -0.090 & 0.025 \\
% rx & 0.068 & 0.017 & -0.044 & 0.012 & -0.036 & 0.018 & -0.073 & 0.017 & 0.066 & 0.017 & -0.011 & 0.016 \\
% ry & -0.194 & 0.018 & 0.009 & 0.012 & 0.060 & 0.019 & 0.020 & 0.018 & -0.019 & 0.018 & 0.054 & 0.018 \\
% rz & 0.126 & 0.019 & -0.006 & 0.015 & -0.115 & 0.018 & 0.022 & 0.018 & 0.100 & 0.018 & 0.048 & 0.018 \\
% lstm256surp & 0.068 & 0.011 & 0.043 & 0.011 & 0.065 & 0.011 & 0.060 & 0.010 & 0.047 & 0.010 & 0.043 & 0.011 \\
% 	   \hline
%
% 	\end{tabular} } % end resizebox
% 	\caption{\label{tab:Acoefs} Regression coefficients ($\pm$ standard error) models with the step-wise addition of LSTM \surprisal fit against data from each ROI.}
% \end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COEFFICIENTS: Add RNNG-COMP SURPRISAL step-wise %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  % \begin{table}[h]
  % 	\centering
  % 	\resizebox{\textwidth}{!}{
  % 	\begin{tabular}{l S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3	]}
  % 		  \hline
  % 		 &
  % 		 \multicolumn{2}{c}{\textbf{LATL}} &
  % 		 \multicolumn{2}{c}{\textbf{RATL}} &
  % 		 \multicolumn{2}{c}{\textbf{LIFG}} &
  % 		 \multicolumn{2}{c}{\textbf{LPTL}} &
  % 		 \multicolumn{2}{c}{\textbf{LIPL}} &
  % 		 \multicolumn{2}{c}{\textbf{LPreM}} \\
  % 		  \hline
  % (Intercept) & -0.046 & 0.052 & -0.075 & 0.026 & 0.176 & 0.049 & -0.073 & 0.054 & 0.061 & 0.059 & 0.127 & 0.048 \\
  % sndpwr & -0.012 & 0.012 & 0.010 & 0.013 & -0.018 & 0.013 & 0.070 & 0.012 & 0.006 & 0.012 & 0.003 & 0.012 \\
  % rate & 0.137 & 0.016 & 0.097 & 0.013 & 0.101 & 0.014 & 0.097 & 0.019 & 0.083 & 0.016 & 0.105 & 0.014 \\
  % freq & 0.020 & 0.011 & 0.035 & 0.012 & -0.015 & 0.012 & 0.012 & 0.011 & 0.056 & 0.011 & 0.045 & 0.011 \\
  % breaks & 0.159 & 0.011 & 0.064 & 0.012 & 0.007 & 0.012 & -0.036 & 0.011 & -0.088 & 0.011 & 0.003 & 0.011 \\
  % dx & 0.118 & 0.018 & 0.008 & 0.012 & 0.160 & 0.018 & 0.286 & 0.017 & 0.309 & 0.017 & 0.223 & 0.018 \\
  % dy & -0.153 & 0.017 & 0.025 & 0.012 & 0.088 & 0.020 & 0.132 & 0.017 & 0.004 & 0.017 & 0.090 & 0.017 \\
  % dz & 0.108 & 0.027 & 0.097 & 0.023 & -0.259 & 0.028 & 0.175 & 0.026 & 0.018 & 0.027 & -0.090 & 0.025 \\
  % rx & 0.068 & 0.017 & -0.044 & 0.012 & -0.035 & 0.018 & -0.073 & 0.017 & 0.066 & 0.017 & -0.011 & 0.016 \\
  % ry & -0.194 & 0.018 & 0.009 & 0.012 & 0.061 & 0.019 & 0.021 & 0.018 & -0.019 & 0.018 & 0.054 & 0.018 \\
  % rz & 0.126 & 0.019 & -0.005 & 0.015 & -0.116 & 0.018 & 0.022 & 0.018 & 0.100 & 0.018 & 0.048 & 0.018 \\
  % lstm256surp & 0.077 & 0.021 & 0.082 & 0.022 & 0.027 & 0.022 & 0.011 & 0.020 & 0.060 & 0.020 & 0.026 & 0.021 \\
  % rnngsurp-comp & -0.011 & 0.021 & -0.046 & 0.022 & 0.045 & 0.022 & 0.058 & 0.020 & -0.015 & 0.020 & 0.021 & 0.021 \\
  % 	   \hline
  %
  % 	\end{tabular} } % end resizebox
  % 	\caption{\label{tab:Bcoefs} Regression coefficients ($\pm$ standard error) models with the step-wise addition of \nocomp \surprisal fit against data from each ROI.}
  % \end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COEFFICIENTS: Add RNNG SURPRISAL step-wise %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \begin{table}[h]
% 	\centering
% 	\resizebox{\textwidth}{!}{
% 	\begin{tabular}{l S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3	]}
% 		  \hline
% 		 &
% 		 \multicolumn{2}{c}{\textbf{LATL}} &
% 		 \multicolumn{2}{c}{\textbf{RATL}} &
% 		 \multicolumn{2}{c}{\textbf{LIFG}} &
% 		 \multicolumn{2}{c}{\textbf{LPTL}} &
% 		 \multicolumn{2}{c}{\textbf{LIPL}} &
% 		 \multicolumn{2}{c}{\textbf{LPreM}} \\
% 		  \hline
% (Intercept) & -0.046 & 0.052 & -0.075 & 0.026 & 0.176 & 0.049 & -0.073 & 0.054 & 0.062 & 0.059 & 0.128 & 0.048 \\
% sndpwr & -0.011 & 0.012 & 0.011 & 0.013 & -0.015 & 0.013 & 0.070 & 0.012 & 0.002 & 0.012 & 0.001 & 0.012 \\
% rate & 0.136 & 0.016 & 0.097 & 0.013 & 0.099 & 0.014 & 0.098 & 0.019 & 0.086 & 0.016 & 0.107 & 0.014 \\
% freq & 0.020 & 0.011 & 0.034 & 0.012 & -0.015 & 0.012 & 0.013 & 0.011 & 0.057 & 0.011 & 0.045 & 0.011 \\
% breaks & 0.158 & 0.012 & 0.062 & 0.012 & 0.002 & 0.013 & -0.034 & 0.011 & -0.079 & 0.011 & 0.006 & 0.012 \\
% dx & 0.118 & 0.018 & 0.008 & 0.012 & 0.160 & 0.018 & 0.286 & 0.017 & 0.309 & 0.017 & 0.223 & 0.018 \\
% dy & -0.153 & 0.017 & 0.025 & 0.012 & 0.088 & 0.020 & 0.132 & 0.017 & 0.004 & 0.017 & 0.090 & 0.017 \\
% dz & 0.108 & 0.027 & 0.097 & 0.023 & -0.259 & 0.028 & 0.175 & 0.026 & 0.017 & 0.027 & -0.091 & 0.025 \\
% rx & 0.068 & 0.017 & -0.044 & 0.012 & -0.036 & 0.018 & -0.073 & 0.017 & 0.066 & 0.017 & -0.011 & 0.016 \\
% ry & -0.194 & 0.018 & 0.009 & 0.012 & 0.060 & 0.019 & 0.021 & 0.018 & -0.019 & 0.018 & 0.055 & 0.018 \\
% rz & 0.126 & 0.019 & -0.005 & 0.015 & -0.116 & 0.018 & 0.022 & 0.018 & 0.101 & 0.018 & 0.048 & 0.018 \\
% lstm256surp & 0.076 & 0.021 & 0.082 & 0.022 & 0.024 & 0.022 & 0.012 & 0.020 & 0.064 & 0.020 & 0.028 & 0.021 \\
% rnngsurp-comp & -0.023 & 0.034 & -0.058 & 0.036 & 0.004 & 0.037 & 0.073 & 0.033 & 0.057 & 0.033 & 0.049 & 0.034 \\
% rnngsurp & 0.014 & 0.031 & 0.014 & 0.033 & 0.048 & 0.033 & -0.017 & 0.030 & -0.082 & 0.030 & -0.032 & 0.031 \\
% 	   \hline
%
% 	\end{tabular} } % end resizebox
% 	\caption{\label{tab:Ccoefs} Regression coefficients ($\pm$ standard error) models with the step-wise addition of full RNNG \surprisal fit against data from each ROI.}
% \end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COEFFICIENTS: Add RNNG-COMP DISTANCE step-wise %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \begin{table}[h]
% 	\centering
% 	\resizebox{\textwidth}{!}{
% 	\begin{tabular}{l S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3	]}
% 		  \hline
% 		 &
% 		 \multicolumn{2}{c}{\textbf{LATL}} &
% 		 \multicolumn{2}{c}{\textbf{RATL}} &
% 		 \multicolumn{2}{c}{\textbf{LIFG}} &
% 		 \multicolumn{2}{c}{\textbf{LPTL}} &
% 		 \multicolumn{2}{c}{\textbf{LIPL}} &
% 		 \multicolumn{2}{c}{\textbf{LPreM}} \\
% 		  \hline
% (Intercept) & -0.045 & 0.052 & -0.075 & 0.026 & 0.177 & 0.049 & -0.073 & 0.054 & 0.061 & 0.058 & 0.127 & 0.048 \\
% sndpwr & -0.014 & 0.012 & 0.008 & 0.013 & -0.027 & 0.013 & 0.057 & 0.012 & -0.001 & 0.012 & -0.001 & 0.012 \\
% rate & 0.140 & 0.016 & 0.100 & 0.013 & 0.107 & 0.014 & 0.106 & 0.019 & 0.089 & 0.016 & 0.108 & 0.014 \\
% freq & 0.021 & 0.011 & 0.038 & 0.012 & -0.019 & 0.012 & 0.008 & 0.011 & 0.057 & 0.011 & 0.043 & 0.011 \\
% breaks & 0.155 & 0.012 & 0.059 & 0.012 & -0.002 & 0.012 & -0.050 & 0.011 & -0.097 & 0.011 & -0.001 & 0.011 \\
% dx & 0.118 & 0.018 & 0.008 & 0.012 & 0.160 & 0.018 & 0.287 & 0.017 & 0.307 & 0.017 & 0.224 & 0.018 \\
% dy & -0.153 & 0.017 & 0.025 & 0.012 & 0.088 & 0.020 & 0.132 & 0.017 & 0.003 & 0.017 & 0.090 & 0.017 \\
% dz & 0.107 & 0.027 & 0.097 & 0.023 & -0.261 & 0.028 & 0.175 & 0.026 & 0.017 & 0.026 & -0.090 & 0.025 \\
% rx & 0.068 & 0.017 & -0.044 & 0.012 & -0.036 & 0.018 & -0.074 & 0.017 & 0.065 & 0.017 & -0.012 & 0.016 \\
% ry & -0.194 & 0.018 & 0.009 & 0.012 & 0.059 & 0.019 & 0.020 & 0.018 & -0.020 & 0.018 & 0.054 & 0.018 \\
% rz & 0.125 & 0.019 & -0.006 & 0.015 & -0.116 & 0.018 & 0.021 & 0.018 & 0.099 & 0.018 & 0.048 & 0.018 \\
% lstm256surp & 0.071 & 0.011 & 0.047 & 0.011 & 0.071 & 0.012 & 0.070 & 0.011 & 0.054 & 0.011 & 0.046 & 0.011 \\
% rnngdist-comp & -0.019 & 0.011 & -0.025 & 0.012 & -0.038 & 0.012 & -0.058 & 0.011 & -0.044 & 0.011 & -0.014 & 0.011 \\
% 	   \hline
%
% 	\end{tabular} } % end resizebox
% 	\caption{\label{tab:Dcoefs} Regression coefficients ($\pm$ standard error) models with the step-wise addition of \nocomp \distance fit against data from each ROI.}
% \end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COEFFICIENTS: Add RNNG DISTANCE step-wise %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \begin{table}[h]
% 	\centering
% 	\resizebox{\textwidth}{!}{
% 	\begin{tabular}{l S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3] S[table-format=1.3]@{$\pm$} S[table-format=1.3	]}
% 		  \hline
% 		 &
% 		 \multicolumn{2}{c}{\textbf{LATL}} &
% 		 \multicolumn{2}{c}{\textbf{RATL}} &
% 		 \multicolumn{2}{c}{\textbf{LIFG}} &
% 		 \multicolumn{2}{c}{\textbf{LPTL}} &
% 		 \multicolumn{2}{c}{\textbf{LIPL}} &
% 		 \multicolumn{2}{c}{\textbf{LPreM}} \\
% 		  \hline
% (Intercept) & -0.047 & 0.052 & -0.075 & 0.026 & 0.176 & 0.049 & -0.075 & 0.054 & 0.060 & 0.058 & 0.127 & 0.048 \\
% sndpwr & 0.001 & 0.012 & 0.010 & 0.013 & -0.018 & 0.013 & 0.071 & 0.012 & 0.004 & 0.012 & 0.005 & 0.012 \\
% rate & 0.134 & 0.016 & 0.100 & 0.013 & 0.104 & 0.014 & 0.101 & 0.019 & 0.087 & 0.016 & 0.105 & 0.014 \\
% freq & 0.013 & 0.011 & 0.037 & 0.012 & -0.023 & 0.012 & 0.001 & 0.011 & 0.055 & 0.011 & 0.040 & 0.011 \\
% breaks & 0.146 & 0.012 & 0.058 & 0.012 & -0.007 & 0.013 & -0.058 & 0.011 & -0.100 & 0.011 & -0.004 & 0.012 \\
% dx & 0.116 & 0.017 & 0.008 & 0.012 & 0.159 & 0.018 & 0.286 & 0.017 & 0.307 & 0.017 & 0.223 & 0.018 \\
% dy & -0.153 & 0.017 & 0.025 & 0.012 & 0.087 & 0.020 & 0.131 & 0.017 & 0.003 & 0.017 & 0.089 & 0.017 \\
% dz & 0.107 & 0.027 & 0.097 & 0.023 & -0.261 & 0.028 & 0.175 & 0.026 & 0.017 & 0.026 & -0.091 & 0.025 \\
% rx & 0.067 & 0.017 & -0.044 & 0.012 & -0.036 & 0.018 & -0.074 & 0.016 & 0.065 & 0.017 & -0.012 & 0.016 \\
% ry & -0.195 & 0.018 & 0.009 & 0.012 & 0.059 & 0.019 & 0.018 & 0.018 & -0.020 & 0.018 & 0.053 & 0.018 \\
% rz & 0.122 & 0.019 & -0.006 & 0.015 & -0.118 & 0.018 & 0.018 & 0.018 & 0.098 & 0.018 & 0.047 & 0.018 \\
% lstm256surp & 0.071 & 0.011 & 0.047 & 0.011 & 0.071 & 0.012 & 0.070 & 0.011 & 0.054 & 0.011 & 0.046 & 0.011 \\
% rnngdist-comp & -0.071 & 0.014 & -0.032 & 0.015 & -0.066 & 0.015 & -0.105 & 0.014 & -0.059 & 0.014 & -0.033 & 0.014 \\
% rnngdist & 0.081 & 0.014 & 0.011 & 0.015 & 0.045 & 0.015 & 0.074 & 0.014 & 0.024 & 0.014 & 0.030 & 0.014 \\
% 	   \hline
%
% 	\end{tabular} } % end resizebox
% 	\caption{\label{tab:Ecoefs} Regression coefficients ($\pm$ standard error) models with the step-wise addition of full RNNG \distance fit against data from each ROI.}
% \end{table}



\bibliographystyle{apalike}
\bibliography{fmri-rnng.bib}





\end{document}
